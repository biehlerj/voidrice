# Voidrice 2: Electric Boogaloo

This repo is a fork of Luke Smith's voidrice. Unlike Luke I am, in his words, a soydev so I need a little more pep in my step to get myself productive on a machine for work and side projects. This repo is a millenial boomer's (tm by me) take on Voidrice that tries to take the best of suckless/minimalist software and modern software development tools. The goal of this is to take everything I love about Luke's setup (suckless software, closely adhering to the Unix philosophy, software freedom/independence, etc.) while also incorporating tools and software that the modern software developer uses on a daily basis (Docker, Kubernetes, JS/TS, Electron, *"Cloud Computing"* like AWS, etc.). I also replace some programs with equivalent or more user friendly or modern alternatives (ie `alacritty` for `st`). I have also blended components of [my dotfiles](https://gitlab.com/biehlerj/dotfiles) that I use on a daily basis with dotfiles here with the hopes that this repo will become my permanent dotfiles repo.

If you have any suggestions for how I can make my dotfiles, scripts, etc. better, if there is a CLI equivalent for a GUI tool I am using, a faster more modern version of a piece of software I'm running that fits in the suckless or Unix philosophy please feel free to open an issue. My goal for this repo is to make it a living breathing repository that can show my journey through my software usage.

## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [From Original Voidrice repo](#from-original-voidrice-repo)
    * [The Voidrice (Luke Smith <https://lukesmith.xyz>'s dotfiles)](#the-voidrice-luke-smith-httpslukesmithxyzs-dotfiles)
    * [Want even more?](#want-even-more)
    * [Install these dotfiles](#install-these-dotfiles)

<!-- vim-markdown-toc -->

## From Original [Voidrice repo](https://github.com/LukeSmithxyz/voidrice)

Everything in this section, with the exception of adding how to install my version of the dotfiles using LARBS, is from the original voidrice repo at the time of me forking it. If there is a major update that I end up using that is noted in the repo I will update it, otherwise everything below this section won't change.

### The Voidrice (Luke Smith <https://lukesmith.xyz>'s dotfiles)

These are the dotfiles deployed by [LARBS](https://larbs.xyz) and as seen on [my YouTube channel](https://youtube.com/c/lukesmithxyz).

- Very useful scripts are in `~/.local/bin/`
- Settings for:
	- vim/nvim (text editor)
	- zsh (shell)
	- i3wm/i3-gaps (window manager)
	- i3blocks (status bar)
	- sxhkd (general key binder)
	- ranger (file manager)
	- lf (file manager)
	- mpd/ncmpcpp (music)
	- sxiv (image/gif viewer)
	- mpv (video player)
	- calcurse (calendar program)
	- tmux
	- other stuff like xdg default programs, inputrc and more, etc.
- I try to minimize what's directly in `~` so:
	- All configs that can be in `~/.config/` are.
	- Some environmental variables have been set in `~/.zprofile` to move configs into `~/.config/`
- Bookmarks in text files used by various scripts (like `~/.local/bin/shortcuts`)
	- File bookmarks in `~/.config/files`
	- Directory bookmarks in `~/.config/directories`

### Want even more?

My setup is pretty modular nowadays.
I use several suckless programs that are meant to be configured and compiled by the user and I also have separate repos for some other things.
Check out their links:

- [dwm](https://github.com/lukesmithxyz/dwm) (the window manager I usually use now which is fully compatible with this repo)
- [st](https://github.com/lukesmithxyz/st) (the terminal emulator assumed to be used by these dotfiles)
- [mutt-wizard (`mw`)](https://github.com/lukesmithxyz/mutt-wizard) - (a terminal-based email system that can store your mail offline without effort)

### Install these dotfiles

Use [LARBS](https://larbs.xyz) to autoinstall everything:

```
curl -LO larbs.xyz/larbs.sh
sh larbs.sh -r "https://gitlab.com/biehlerj/voidrice.git"
```

or clone the repo files directly to your home directory and install [the prerequisite programs](https://github.com/LukeSmithxyz/LARBS/blob/master/progs.csv).
