import requests
from rich.console import Console
from rich.table import Table

console = Console()

states_of_note = ["California", "Texas", "Arizona", "New Hampshire"]

covid19_table = Table(show_header=True, header_style="bold purple", show_lines=True)
covid19_table.add_column("State")
covid19_table.add_column("Total Cases")
covid19_table.add_column("Resolved Cases")
covid19_table.add_column("Deaths")
covid19_table.add_column("Active Cases")
covid19_table.add_column("New Cases")
covid19_table.add_column("New Deaths")
covid19_table.add_column("Survival Rate")
covid19_table.add_column("Death Rate")
covid19_table.add_column("Total Tests")
covid19_table.add_column("Tests per 1 Million People")

try:
    covid_stats = requests.get("https://corona-stats.online/states/us?format=json")
except requests.ConnectionError:
    print("No active internet connection, please try again later")
    exit(1)
stats_by_state = covid_stats.json()["data"]

for i, v in enumerate(stats_by_state):
    if v["state"] in states_of_note:
        total_resolved_cases = v["cases"] - v["active"]
        death_rate = v["deaths"] / total_resolved_cases * 100
        survival_rate = (
            (total_resolved_cases - v["deaths"]) / total_resolved_cases
        ) * 100
        covid19_table.add_row(
            v["state"],
            f"{v['cases']:,}",
            f"{total_resolved_cases:,}",
            f"{v['deaths']:,}",
            f"{v['active']:,}",
            f"{v['todayCases']:,}",
            f"{v['todayDeaths']:,}",
            f"{survival_rate:.3f}%",
            f"{death_rate:.3f}%",
            f"{v['tests']:,}",
            f"{v['testsPerOneMillion']:,}",
        )

console.print(covid19_table)
